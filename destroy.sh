cd /var/www/html

wp --allow-root plugin delete wp-mail-logging
wp --allow-root plugin delete debug-bar
wp --allow-root plugin delete debug-bar-actions-and-filters-addon
wp --allow-root plugin delete debug-bar-slow-actions

rm -rf wp-admin
rm -rf wp-includes
rm wp-*.php
rm .htaccess
rm index.php
rm license.txt
rm xmlrpc.php
rm readme.html

mysqldump -uroot -proot wordpress > wordpress.sql
