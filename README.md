# Vagrant Wordpress

## Requirements
- Vagrant
- CURL

## Usage

http://127.0.0.1:8080/wp-login.php (admin:admin)

http://127.0.0.1:8080/phpmyadmin (root:root)

## Quick Install

#### macOS or Unix

```bash
curl -#L https://gitlab.com/michaelmarkie/vagrant-wordpress/raw/master/shortcut.sh | sh
```

#### Windows 10 > 1803
```bash
curl -# -o 'install.sh' https://gitlab.com/michaelmarkie/vagrant-wordpress/raw/master/install.sh
curl -# -o 'destroy.sh' https://gitlab.com/michaelmarkie/vagrant-wordpress/raw/master/destroy.sh
curl -# -o 'Vagrantfile' https://gitlab.com/michaelmarkie/vagrant-wordpress/raw/master/Vagrantfile
vagrant up
```

#### Other Windows
You require CURL to be installed this can be done through your package manager or if you have installed Git you should have it and can use the above command.


## Manual Install
- Download zip
- Put in the directory you wish
- Run:
```bash
vagrant up
```

## Debugging

PHP errors
```bash
vagrant ssh
sudo tail -f /var/log/apache2/error.log
```