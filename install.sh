sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

sudo add-apt-repository -y ppa:ondrej/php
sudo apt-add-repository -y ppa:ondrej/apache2
sudo apt-get update

sudo apt-get install -y curl python-software-properties
sudo apt-get install -y php apache2 libapache2-mod-php php-curl php-gd php-readline
sudo apt-get install -y mysql-server php-mysql

# Allow External Connections on your MySQL Service
sudo sed -i -e 's/bind-addres/#bind-address/g' /etc/mysql/mysql.conf.d/mysqld.cnf
sudo sed -i -e 's/skip-external-locking/#skip-external-locking/g' /etc/mysql/mysql.conf.d/mysqld.cnf
mysql -u root -proot -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root'; FLUSH privileges;"
sudo service mysql restart

sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password root"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin

# phpmyadmin error fix: https://stackoverflow.com/a/50536059
sudo sed -i "s/|\s*\((count(\$analyzed_sql_results\['select_expr'\]\)/| (\1)/g" /usr/share/phpmyadmin/libraries/sql.lib.php

# auto login
sudo sed -i "s/\$cfg\['Servers'\]\\[\$i\]\['auth_type'\] = 'cookie';/\$cfg\['Servers'\]\[\$i\]\['auth_type'\] = 'config';\n\t\$cfg\['Servers'\]\[\$i\]\['username'\] = 'root';\n\t\$cfg\['Servers'\]\[\$i\]\['password'\] = 'root';\n\t\$cfg\['LoginCookieValidity'\] = 1440;/" /etc/phpmyadmin/config.inc.php

echo "CREATE DATABASE IF NOT EXISTS wordpress" | mysql -uroot -proot
if [ -f /var/www/html/wordpress.sql ]; then
    mysql -uroot -proot wordpress < /var/www/html/wordpress.sql
fi

sudo a2enmod rewrite


# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/html"
    <Directory "/var/www/html">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

sudo service apache2 restart

cd /var/www/html
rm index.html
curl -sS -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

wp --allow-root core download

HTA=$(cat <<EOF
# BEGIN WordPress

RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]

# END WordPress
EOF
)
echo "${HTA}" > .htaccess

mv wp-config-sample.php wp-config.php
sudo sed -i "s/define( 'DB_NAME', '.*' );/define( 'DB_NAME', 'wordpress' );/" wp-config.php
sudo sed -i "s/define( 'DB_USER', '.*' );/define( 'DB_USER', 'root' );/" wp-config.php
sudo sed -i "s/define( 'DB_PASSWORD', '.*' );/define( 'DB_PASSWORD', 'root' );/" wp-config.php
sudo sed -i "s/define( 'WP_DEBUG', .* );/define( 'WP_DEBUG', true );\ndefine( 'SAVEQUERIES', true );/" wp-config.php
echo -e "\ndefine('FS_METHOD','direct');" | sudo tee -a wp-config.php

wp --allow-root core install --url=127.0.0.1:8080 --title=vagrant --admin_user=admin --admin_password=admin --admin_email=root@localhost.local

wp --allow-root plugin delete akismet
wp --allow-root plugin delete hello

wp --allow-root plugin install wp-mail-logging --activate
wp --allow-root plugin install debug-bar --activate
wp --allow-root plugin install debug-bar-actions-and-filters-addon --activate
wp --allow-root plugin install debug-bar-slow-actions --activate

echo 'http://127.0.0.1:8080/wp-login.php - admin:admin'